//This migration is basically changing the state of the blockchain basically. Eg similar to change the state of the 
//database, basically adding a col or deleting etc, this is what a migration does for a blockchain.
var TodoList  = artifacts.require("./TodoList.sol");

module.exports = function(deployer) {
  deployer.deploy(TodoList);
};